import requests
import re
import logging
import json
import time
import os
from bs4 import BeautifulSoup

URLLIB_RETRY_DELAY = os.environ.get('SCRAPER_URLLIB_RETRY_DELAY', 1)
URLLIB_RETRY_MAX = os.environ.get('SCRAPER_URLLIB_RETRY_MAX', 15)
SHOP_URL = "https://www.idealo.de"
API_OFFER_URL = "https://www.idealo.de/offerpage/offerlist/product/{product_id}/start/{product_scope}/sort/default?includeFilters=0&excludeFilters=2062"
OFFER_SCOPE_SIZE = 15

def get_url_data(page_url: str, sanitize: bool=True, try_cpt: int=1) -> str:
    """
    Get web page from url
    :param page_url: <string>
    :param sanitize: <boolean> Clean raw data. Default=True
    :param try_cpt: <int> Avoid infinity loop. Default=1
    :return: <string> web page from URL
    """
    try:
        web_page = requests.get(page_url, allow_redirects=False)
    except requests.exceptions.HTTPError as e:
        # Recursive loop (max=URLLIB_RETRY_MAX, Endless if -1)
        # Connection to url may fail. Wait x secondes and retry.
        if try_cpt < URLLIB_RETRY_MAX or URLLIB_RETRY_MAX == -1:
            time.sleep(URLLIB_RETRY_DELAY)
            return get_url_data(page_url, sanitize=sanitize, try_cpt=try_cpt+1) 
        raise e
    return sanitize and sanitize_raw_data(web_page.content) or web_page

def sanitize_raw_data(raw_data: str) -> str:
    """
    Clean raw data, remove useless character 
    :param raw_data: <string>
    :return: <string>
    """
    raw_data = bytes(raw_data).decode("UTF-8")
    return raw_data.replace('&#034;', '"').replace('\n', '').replace('\t', '').replace('&amp;', '&').replace('\xa0', ' ').replace('\0xe4', 'a')

def get_offer_information(product_id: str) -> list:
    """
    Get and parse the offer list
    :param product_id: <string>
    :return: <list> of <dictionary> List of offers data
    """
    res = []
    product_scope = 0
    while True:
        raw_data = get_url_data(API_OFFER_URL.format(product_id=product_id, product_scope=product_scope))
        data = BeautifulSoup(raw_data, "html.parser")

        product_handled_nb = 0
        for product_data in data.find_all('li', 'productOffers-listItem'):
            offer_link = product_data.find('div', 'productOffers-listItemTitleWrapper').find('a', 'productOffers-listItemTitle')
            offer_data = json.loads(offer_link.get('data-gtm-payload'))

            try:
                shop_url = get_url_data(f"{SHOP_URL}{offer_link.get('href')}")
                shop_url = shop_url.status_code == 301 and shop_url.headers.get('Location', False) or shop_url.url
            except Exception as e:
                logging.error(e)
                shop_url = False

            offer = {
                'shop_name': offer_data.get('shop_name', False),
                'price': float(offer_data.get('product_price', False)),
                'url': shop_url or False,
                'asin': False
            }

            get_product_asin(offer)
            
            res.append(offer)
            
            product_handled_nb += 1
            logging.info(f"New offer found: {offer_data.get('shop_name', False)} - {offer_data.get('product_price', False)}")
        
        product_scope += product_handled_nb
        if product_handled_nb < OFFER_SCOPE_SIZE:
            break
    
    logging.info(f"Number of offers: {len(res)}")
    return res

def get_product_id(product_url: str) -> str:
    """
    Search and return the product id based on the product url
    :param product_url: <string> url.
    :return: <string> Product Id
    """
    logging.info("Get Product Id")
    product_id_regex = r'^https?://www.idealo.de\/.*OffersOfProduct\/(\d*)\D?.*$'
    product_id_re_group = re.search(product_id_regex, product_url)
    if product_id_re_group:
        return product_id_re_group.group(1)
    return False

def get_product_asin(model_data: dict) -> bool:
    """
    Search and return product asin for amazon url
    :param model_data: <string>
    :return: <string>
    """    
    if re.search(r"^http:\/\/www\.amazon\.de.*$", model_data.get('url', False) or ''):
        asin = re.search(r"http:\/\/www.amazon.de\/dp\/(\S*)(\/|\?m|\?s).*$", model_data.get('url', ''))
        if asin:
            model_data.update({'asin': asin.group(1)})
        else:
            logging.error(f"Can't match amazon url to find ASIN: {model_data.get('url')}")

def get_product_data(product_url: str) -> dict:
    """
    Search and return product data.
    :param product_url: <string> url.
    :return: <dictionary> Product Data
    """
    raw_product_data = get_url_data(product_url)
    product_data = BeautifulSoup(raw_product_data, "html.parser")

    # Search Name of Product
    product_name = product_data.find("div", "oopStage-details-header").find("span").text

    # Search Number of Offer
    number_offer = product_data.find("div", "oopStage-metaInfo row table").find("span", "table-cell oopStage-priceRangeOffers").text
    number_offer = int(re.search(r"^([0-9]*) Angebote:$", number_offer).group(1))  # TODO add try except

    # Search Price Range
    price_range = product_data.find("div", "oopStage-metaInfo row table").find("span", "table-cell oopStage-priceRangePrice").text
    price_range = [float(price.replace(',', '.')) for price in re.findall(r"([0-9]{2}.[0-9]{2})", price_range)]

    return {
        'product_id': get_product_id(product_url),
        'name': product_name, 
        'number_offer': number_offer,
        'min_price': min(price_range),
        'max_price': max(price_range),
    }

def scraper_idealo_de(product_url: str) -> dict:
    """
    Call the scraper for www.idealo.de product.
    :param product_url: <string>
    :return: <dictionary>
    """
    logging.info("Main Function")
    product_data = get_product_data(product_url)
    
    # Get offer information
    product_data.update({'offer_list': get_offer_information(product_data['product_id'])})
    
    return product_data

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # URL For Testing
    main_product_list = [
        "https://www.idealo.de/preisvergleich/OffersOfProduct/5863092_-ice-ola-kids-s-ice-watch.html",
        "https://www.idealo.de/preisvergleich/OffersOfProduct/4759339_-parker-chrono-michael-kors.html",
        "https://www.idealo.de/preisvergleich/OffersOfProduct/5297284_-super-robuste-strumpfhose-bataillon-belette.html",
    ]

    # Save result into file
    with open(f"product.json", "w") as file:
        for product_url in main_product_list:
            product_data = scraper_idealo_de(product_url=product_url)
            file.write(json.dumps(product_data))
