# Installation:
    python3.7 -m pip install -r requirements.txt

# Run Server
    python3.7 app.py

# Get product data
    curl -X GET 'http://localhost:5000?url=https://www.idealo.de/preisvergleich/OffersOfProduct/5863092_-ice-ola-kids-s-ice-watch.html'
