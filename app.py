import json
from flask import Flask, request
from scraper_idealo_de import scraper_idealo_de

app = Flask(__name__)

@app.route('/', methods=['GET'])
def main_route():
    if request.args.get('url', False):
        res = scraper_idealo_de(request.args.get('url'))
        return json.dumps(res)
    return ''
if __name__ == '__main__':
    app.run('0.0.0.0')
